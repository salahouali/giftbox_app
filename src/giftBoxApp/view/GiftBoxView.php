<?php
namespace giftBoxApp\view;

use giftBoxApp\model\Categorie;

class GiftBoxView extends \mf\view\AbstractView {
  
    public function __construct( $data ){
        parent::__construct($data);
    }


    private function renderHeader(){
        return '<h1>GiftBoxApp</h1>';
    }
    

    private function renderFooter(){
        return 'La super app créée en Licence Pro &copy;2018';
    }

    
    private function renderPrestations(){

        $router = new \mf\router\Router(); 
        $app_root = (new \mf\utils\HttpRequest())->root;       

        $res = "<article class='row'>";
        $res .= "<h2 class='s12 m12 l12'>Liste des prestations</h2>"; 

        $res .= "<section class='col s12 m3 l3 row'>";
        $res .= "<ul class='cat col s12 m12 l12 row'>";

        foreach($this->data['categories'] as $key => $t){

            $res .= "<li class='col s3 m12 l12'><a href='".$router->urlFor('categorie',['id' => $t->id])."'>$t->nom</a></li>";
        }



        $res .= "</ul></section>";

        $res .= "<section class='col s12 m9 l9 row'>";

        foreach($this->data['prestations'] as $key => $t){

            $res .= "<div class='col s12 m6 l4 prestation'>";
            $res .= "<div class='row pres_info'>";
            $res .= "<span class='col s10 m10 l10'>$t->nom</span>";
            $res .= "<p class='col s2 m2 l2'>$t->prix</p>";
            $res .= "</div>";
            $res .= "<img class='imgPres' src='$app_root/html/images/$t->img' />";
            $res .= "</div>";
        }

        $res .= "</section>";
        $res .= "</article>";
        
        return $res;
    }

    private function renderCategoriePrestations(){
        $router = new \mf\router\Router(); 
        $app_root = (new \mf\utils\HttpRequest())->root; 
        
        if(!is_null($this->data['categorie'])){
            $res = "<article class='row'>";
            $res .= "<h2 class='col s12 m12 l12'>".$this->data['categorie']->nom."</h2>";

            $res .= "<section class='col s12 m3 l3 row'>";
            $res .= "<ul class='cat col s12 m12 l12 row'>";

            foreach($this->data['categories'] as $key => $t){

                $res .= "<li class='";
                    if($t->id == $this->data['categorie']->id)
                    $res .= "active";
                $res .= " col s3 m12 l12'><a href='".$router->urlFor('categorie',['id' => $t->id])."'>$t->nom</a></li>";
            }

            $res .= "</ul></section>";

            $res .= "<section class='col s12 m9 l9 row'>";

                foreach($this->data['prestations'] as $key => $t){

                    $res .= "<div class='col s12 m6 l4 prestation'>";
                    $res .= "<div class='row pres_info'>";
                    $res .= "<span class='col s10 m10 l10'>$t->nom</span>";
                    $res .= "<p class='col s2 m2 l2'>$t->prix</p>";
                    $res .= "</div>";
                    $res .= "<img class='imgPres' src='$app_root/html/images/$t->img' />";
                    $res .= "</div>";
                }
        
                $res .= "</section>";
                $res .= "</article>";

            return $res;
            }else{
                return;
            }
    }
    
    private function renderRegister(){
        $res = "<form action='index.php/main.php/addUser/' method='post'> ".
        "<label>Email </label>".
        "<input class='input' type='email' name='email' id='rpassword' placeholder='email' required /><br>".
        "<label>Nom </label>".
        "<input class='input' type='text' name='fullname' id='fullname' placeholder='full name' required /><br>".
        "<label>Prenom </label>".
        "<input class='input' type='text' name='lastname' id='lastname' placeholder='last name' required /><br>".
        "<label>Prenom </label>".
        "<input class='input' type='test' name='username' id='username' placeholder='username' required /><br>".
        "<label>Mot de passe </label>".
        "<input class='input' type='password' name='password' id='password' placeholder='password' required /><br>".
        "<label>Confirmation mdp </label>".
        "<input class='input' type='text' name='rpassword' id='rpassword' placeholder='retype password' required /><br>". 
        "<input class='button input' type='submit' value='Register' id='register' name='register' /></form>";

        return $res;
    }
    

    private function renderViewSignIn(){

        $res = "<form> ".       
        "<input class='input' type='text' name='fullname' id='fullname' placeholder='full name' required /><br>".
        "<input class='input' type='test' name='username' id='username' placeholder='username' required /><br>".
        "<input class='input' type='password' name='password' id='password' placeholder='password' required /><br>".
        "<input class='input' type='text' name='rpassword' id='rpassword' placeholder='retype password' required /><br>".  
        "<input class='button input' type='submit' value='Sign in' id='signin' name='signin' /></form>";

        return $res;
    }

    private function renderViewFormulaire(){
        $router = new \mf\router\Router();
        
        $res = "<form id='tweet-form' class='forms' method='POST' action='".$router->urlFor('send')."'> ".       
        "<textarea name='textarea' id='textarea' placeholder='Enter tweet...'></textarea><br>".  
        "<input class='button input' type='submit' value='Send' id='' name='send' /></form>";

        return $res;
    }


    
    protected function renderBody($selector=null){

        /*
         * voire la classe AbstractView
         * 
         */
         $http_req = new \mf\utils\HttpRequest();

         $res = "";

         $res .= "<header class='theme-backcolor1'>".$this->renderHeader();

         $res .= "<nav id='nav-menu'>";

        //  $res .= "<a class='button' href='$http_req->script_name'>HOME</a>";
        //  $res .= "<a class='button' href='$http_req->script_name'>LOGIN</a>";
        //  $res .= "<a class='button' href='$http_req->script_name'>ADD</a>";

         $res .= "</nav></header>";

         $res .= "<section>";

         if($selector == 'prestations')
         $res .= "<section>".$this->renderPrestations()."</section>";

         if($selector == 'categoriePrestations')
         $res .= "<section>".$this->renderCategoriePrestations()."</section>";

         if($selector == 'register')
         $res .= "<section>".$this->renderRegister()."</section>";

       

         $res .= "</section>";

         $res .= "<footer class='theme-backcolor1'>".$this->renderFooter()."</footer>";

         return $res;
    }











    
}

?>
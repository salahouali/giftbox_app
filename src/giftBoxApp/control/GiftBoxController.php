<?php
namespace giftBoxApp\control;

use giftBoxApp\model\Prestation;
use giftBoxApp\model\Categorie;
use giftBoxApp\model\User;



class GiftBoxController extends \mf\control\AbstractController {

    
    public function __construct(){
        parent::__construct();
    }

    
    public function viewPrestations(){

         $prestations = Prestation::select()->get();
         $categories = Categorie::select()->get();
        
         $vue = new \giftBoxApp\view\GiftBoxView([
            'categories' => $categories,
            'prestations' => $prestations
            ]);

         return $vue->render('prestations');

    }

    public function viewRegister() {

        $vue = new \giftBoxApp\view\GiftBoxView(null);
        return $vue->render('register');
    }

    public function viewAddUser(){
        if (isset($_POST['register'])) {
            $user = new User;
            $user->email = $_POST['email'];
            $user->nom = $_POST['fullname'];
            $user->prenom = $_POST['lastname'];
            $user->login = $_POST['username'];
            $user->pass = $_POST['pass'];
            $user->save();
        }
        $vue = new \giftBoxApp\view\GiftBoxView();
    }

    public function viewCategoriePrestations(){

        if(isset($_GET['id'])){

            $cat_id = $_GET['id'];
            $categories = Categorie::select()->get();
            $categorie = Categorie::where('id','=',$cat_id)->first();
            if(!is_null($categorie))
            $prestations = $categorie->prestations()->get();
            else{
             $prestations = null;
            }
 
            $vue = new \giftBoxApp\view\GiftBoxView([
                'categorie' => $categorie,
                'categories' => $categories,
                'prestations' => $prestations
                ]);
 
            return $vue->render('categoriePrestations');  
 
         }else{

         }

    }


}
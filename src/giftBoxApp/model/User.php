<?php
namespace giftBoxApp\model;

class Prestation extends \Illuminate\Database\Eloquent\Model {

       protected $table      = 'user';  
       protected $primaryKey = 'id';     
       public    $timestamps = false;  
       
       public function coffret()
       {
           return $this->hasMany('giftBoxApp\model\Coffret', 'id_coffret');
       }

       
}
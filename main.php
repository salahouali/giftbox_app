<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* pour le chargement automatique des classes dans vendor */
require_once 'vendor/autoload.php';

require_once 'src/mf/utils/ClassLoader.php';

$loader = new mf\utils\ClassLoader("src");
$loader->register();

use mf\auth\Authentification;
use mf\router\Router;







$init = parse_ini_file("conf/config.ini");

$config = [
    'driver'    => $init["type"],
    'host'      => $init["host"],
    'database'  => $init["name"],
    'username'  => $init["user"],
    'password'  => $init["pass"],
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '' ];

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */


$router = new \mf\router\Router();

$router->addRoute('prestations', '/prestations/', '\giftBoxApp\control\GiftBoxController', 'viewPrestations');
$router->addRoute('categorie', '/categorie/', '\giftBoxApp\control\GiftBoxController', 'viewCategoriePrestations');
$router->addRoute('register', '/register/', '\giftBoxApp\control\GiftBoxController', 'viewRegister');

$router->setDefaultRoute('/prestations/');

$router->run();


?>